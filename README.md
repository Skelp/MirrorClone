
# [<img src="/misc/icon.png" width="32" height="32">](/icon.png) MirrorClone [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) ![Nuget](https://img.shields.io/nuget/v/Skelp.MirrorClone) 

This repository provides a reflection-based cloner which copies over anything it can find while also considering the original object references (eg. two child-objects having the same reference to the parent object). 

**No class-attributes required.**

## Table of Contents

1. [Supported Types](#supported-types)
2. [Installation](#installation)
3. [Why MirrorClone?](#why-mirrorclone)
4. [Getting Started](#getting-started)

## Supported Types

Generally speaking, **anything** should be clonable.

However, instances of the following have been covered by tests and are therefore guaranteed to work.

* primitives
* strings
* classes
* structs
* arrays (inluding ragged- and multidimensional arrays)
* collections (`List`, `Dictionary`)
* delegates (`Func`)
* null
* dynamic (`ExpandoObject`)

> Anything in round brackets represent the implementing type used in the tests

Anything above can be cloned as a member of another (more complex) object or on their own.
The only thing known **not** to be cloned are **static members** - this is due to the nature of their existance and cannot be cloned.

Should you find something which **refuses** to be cloned, please raise an issue in this repository.

## Installation

Simply use the Visual Studio NuGet Package Manager and look for `MirrorClone`.

Alternatively, you can use the Package Manager Console as such:
> `Install-Package Skelp.MirrorClone`

If you want to use the .NET CLI, you can use the following command:
> `dotnet add package Skelp.MirrorClone`

## Why MirrorClone?

There are many cloning libraries on NuGet, indeed. Originally I planned to use one of them for a project of mine.
Soon I had to conclude that I would have to write my own cloner.

The reason? 
Other cloners are **not reliable** in terms of references. Cloned objects should be cloned in their entirety, otherwise a MemberwiseClone() or manual cloning would suffice. Down below is a small table of tested packages, only showing the differences, not a complete list of their features.

| |Version| Structs | w/ Ref. Members | Looping Ref. | Dynamic |
|--|--|--|--|--|--|
|**MirrorClone**|1.1.0|✅|✅|✅|✅|
|**AnyClone**|1.1.2|✅|❌|✅|✅|
|**CloneExtensions**|1.4.2|✅|❌|✅|❌|
|**DeepCloner**|0.10.3|✅|❌|✅|✅|
|**FastDeepCloner**|1.3.6|❌|❌|❌|❌|


> An ❌ either represents a thrown exception on cloning or an inaccuracy of a member in the clone

**Note** that none of the above packages are any less valuable for lacking certain features. However, my specific application called for a complete, reliable deep clone of the underlying type which is _completely_ decoupled from the original object. 

Another reason is the amount of features. Personally, I prefer packages that keep it simple: One problem, one solution.
Most other packages add features such as `ignore attributes` in order to ignore cloning marked members.

MirrorClone only provides what is needed to clone an entire object.
This has the unintentional benefit of (in some cases) being less resource intensive, both CPU and memory wise.

> Plans are made to provide a benchmark result for various scenarios to showcase the above

## Getting Started

The package consists of of two namespaces.

1. **Skelp.MirrorClone** consists of the actual `Cloner` class
2. **Skelp.MirrorClone.Extensions** provides `object` extensions

Choose either of them to get started.

### Using The `object` Extensions

```csharp
using Skelp.MirrorClone.Extensions;
```
`[...]`
```csharp
YourObject objectToClone = new YourObject();

// This is what will be called 99.9% of the time
var clonedObject = objectToClone.MirrorClone();

// Alternatively, if object is deeply nested and presents stack overflow concerns
// this will catch any stack overflow from crashing the application
bool success = objectToClone.TryMirrorClone(out YourObject clonedObject);
```

### Using The `Cloner` Class

```csharp
using Skelp.MirrorClone;
```
`[...]`
```csharp
YourObject objectToClone = new YourObject();

var cloner = new Cloner();

// This is what will be called 99.9% of the time
var clonedObject = cloner.Clone(objectToClone);

// Alternatively, if object is deeply nested and presents stack overflow concerns
// this will catch any stack overflow from crashing the application
bool success = cloner.TryClone(objectToClone, out YourObject clonedObject);
```
