﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone
{
    /// <summary>
    /// Class providing deep, reference-safe cloning via Reflection.
    /// </summary>
    public class Cloner
    {
        private readonly Dictionary<object, object> referenceDictionary = new Dictionary<object, object>(ReferenceEqualityComparer.Instance);

        /// <summary>
        /// Clones an object.
        /// </summary>
        /// <param name="instance">Anything, including null.</param>
        /// <returns>An exact copy of <paramref name="instance"/>.</returns>
        /// <exception cref="InsufficientExecutionStackException">
        /// Thrown when <paramref name="instance"/> is exhibiting too large of a depth and nearly causes a stack overflow.
        /// </exception>
        public object Clone(object instance)
        {
            var result = LazyClone(instance);
            referenceDictionary.Clear();
            return result;
        }

        /// <summary>
        /// Clones an object.
        /// </summary>
        /// <typeparam name="T">Any type.</typeparam>
        /// <param name="instance">Anything, including null.</param>
        /// <returns>An exact copy of <paramref name="instance"/>.</returns>
        /// <exception cref="InsufficientExecutionStackException">
        /// Thrown when <paramref name="instance"/> is exhibiting too large of a depth and nearly causes a stack overflow.
        /// </exception>
        public T Clone<T>(T instance)
        {
            return (T)Clone((object)instance);
        }

        /// <summary>
        /// Attempts to clone an object.
        /// </summary>
        /// <param name="instance">Anything, including null.</param>
        /// <param name="clone">The cloned object.</param>
        /// <returns>True if cloning was successful and <paramref name="clone"/> is valid, false otherwise.</returns>
        public bool TryClone(object instance, out object clone)
        {
            try
            {
                clone = Clone(instance);
                return true;
            }
            catch (InsufficientExecutionStackException)
            {
                clone = null;
                return false;
            }
        }

        /// <summary>
        /// Attempts to clone an object.
        /// </summary>
        /// <typeparam name="T">Any type.</typeparam>
        /// <param name="instance">Anything, including null.</param>
        /// <param name="clone">The cloned object.</param>
        /// <returns>True if cloning was successful and <paramref name="clone"/> is valid, false otherwise.</returns>
        public bool TryClone<T>(T instance, out T clone)
        {
            try
            {
                clone = (T)Clone((object)instance);
                return true;
            }
            catch (InsufficientExecutionStackException)
            {
                clone = default;
                return false;
            }
        }

        internal object LazyClone(object instance)
        {
            if (TryGetEasyResult(instance, out object easyResult))
            {
                return easyResult;
            }
            else
            {
                return CloneComplexObject(instance);
            }
        }

        internal object CloneComplexObject(object instance)
        {
            if (instance is Array array)
            {
                if (array.Rank == 1)
                {
                    return HandlePlainArray(array);
                }
                else
                {
                    return HandleMultiRankArray(array);
                }
            }
#if NET6_0_OR_GREATER
            if (instance is Delegate @delegate)
            {
                return HandleDelegate(@delegate);
            }
#endif
            else
            {
                return HandleComplexObject(instance);
            }
        }

        private bool TryGetEasyResult(object instance, out object clone)
        {
            clone = null;

            if (instance == null)
            {
                return true;
            }

            var type = instance.GetType();

            if (type.IsPrimitive || type.IsEnum || type.Equals(typeof(string)))
            {
                clone = instance;
                return true;
            }

            if (type.IsValueType && TypeEvaluator.IsStructWithoutReferenceTypes(type))
            {
                clone = instance;
                return true;
            }

            if (referenceDictionary.TryGetValue(instance, out object value))
            {
                clone = value;
                return true;
            }

            return false;
        }

        private object HandlePlainArray(Array array)
        {
            var arrayElementType = array.GetType().GetElementType();
            var clonedArray = Array.CreateInstance(arrayElementType, array.Length);

            if (arrayElementType.IsPrimitive || typeof(string).Equals(arrayElementType))
            {
                Array.Copy(array, clonedArray, array.Length);
                return clonedArray;
            }

            for (int i = 0; i < array.Length; i++)
            {
                var value = array.GetValue(i);

                if (TryGetEasyResult(value, out object arrayItemResult))
                {
                    clonedArray.SetValue(arrayItemResult, i);
                }
                else
                {
                    RuntimeHelpers.EnsureSufficientExecutionStack();
                    clonedArray.SetValue(CloneComplexObject(value), i);
                }
            }

            return clonedArray;
        }

        private object HandleMultiRankArray(Array array)
        {
            var ranks = array.Rank;
            var arrayElementType = array.GetType().GetElementType();

            var rankLengths = new long[ranks];

            for (int i = 0; i < ranks; i++)
            {
                rankLengths[i] = array.GetLongLength(i);
            }

            var clonedArray = Array.CreateInstance(arrayElementType, rankLengths);

            if (arrayElementType.IsPrimitive)
            {
                Array.Copy(array, clonedArray, array.Length);
                return clonedArray;
            }

            var currentIndecies = new long[ranks];

            for (int iRank = 0; iRank < ranks; iRank++)
            {
                for (int iRankLen = 0; iRankLen < rankLengths[iRank]; iRankLen++)
                {
                    for (int iInner = iRank + 1; iInner < ranks; iInner++)
                    {
                        currentIndecies[iRank] = iRankLen;

                        for (int iInnerLen = 0; iInnerLen < rankLengths[iInner]; iInnerLen++)
                        {
                            currentIndecies[iInner] = iInnerLen;

                            var currentValue = array.GetValue(currentIndecies);

                            if (TryGetEasyResult(currentValue, out object arrayItemResult))
                            {
                                clonedArray.SetValue(arrayItemResult, currentIndecies);
                            }
                            else
                            {
                                RuntimeHelpers.EnsureSufficientExecutionStack();
                                clonedArray.SetValue(CloneComplexObject(currentValue), currentIndecies);
                            }
                        }
                    }
                }
            }

            return clonedArray;
        }

        private object HandleComplexObject(object instance)
        {
            var type = instance.GetType();
            object clonedObject = RuntimeHelpers.GetUninitializedObject(type);
            referenceDictionary.Add(instance, clonedObject);
            CloneFields(type, instance, ref clonedObject);
            return clonedObject;
        }

        private void CloneFields(Type type, object original, ref object clonedObject)
        {
            var fieldInfos = type.GetRuntimeFields();

            foreach (var fieldInfo in fieldInfos)
            {
                var fieldType = fieldInfo.FieldType;

                if (fieldInfo.IsStatic && fieldInfo.IsInitOnly)
                {
                    continue;
                }

                if (fieldType.IsPrimitive || fieldType.IsEnum || fieldType.Equals(typeof(string)))
                {
                    fieldInfo.SetValue(clonedObject, fieldInfo.GetValue(original));
                }
                else
                {
                    var currentValue = fieldInfo.GetValue(original);

                    if (currentValue == null)
                    {
                        fieldInfo.SetValue(clonedObject, null);
                    }
                    else if ((fieldInfo.IsStatic && fieldInfo.IsInitOnly) == false)
                    {
                        if (TryGetEasyResult(currentValue, out object fieldResult))
                        {
                            fieldInfo.SetValue(clonedObject, fieldResult);
                        }
                        else
                        {
                            RuntimeHelpers.EnsureSufficientExecutionStack();
                            fieldInfo.SetValue(clonedObject, CloneComplexObject(currentValue));
                        }
                    }
                }
            }
        }

#if NET6_0_OR_GREATER
        private object HandleDelegate(Delegate @delegate)
        {
            // Shallow copy only
            var delegateClone = @delegate.Clone();
            referenceDictionary.Add(@delegate, delegateClone);

            // Doing what microsoft won't - deep copying a delegate
            CloneFields(delegateClone.GetType(), @delegate, ref delegateClone);
            return delegateClone;
        }
#endif
    }
}