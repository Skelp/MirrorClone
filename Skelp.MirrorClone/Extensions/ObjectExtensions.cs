﻿using System;

namespace Skelp.MirrorClone.Extensions
{
    /// <summary>
    /// Extension class providing cloning methods to any object.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Clones an object.
        /// </summary>
        /// <typeparam name="T">Any type.</typeparam>
        /// <param name="instance">Anything, including null.</param>
        /// <returns>An exact copy of <paramref name="instance"/>.</returns>
        /// <exception cref="InsufficientExecutionStackException">
        /// Thrown when <paramref name="instance"/> is exhibiting too large of a depth and nearly causes a stack overflow.
        /// </exception>
        public static T MirrorClone<T>(this T instance)
        {
            return (T)new Cloner().LazyClone(instance);
        }

        /// <summary>
        /// Attempts to clone an object.
        /// </summary>
        /// <typeparam name="T">Any type.</typeparam>
        /// <param name="instance">Anything, including null.</param>
        /// <param name="clone">The cloned object.</param>
        /// <returns>True if cloning was successful and <paramref name="clone"/> is valid, false otherwise.</returns>
        public static bool TryMirrorClone<T>(this T instance, out T clone)
        {
            var cloner = new Cloner();

            try
            {
                clone = (T)cloner.LazyClone(instance);
                return true;
            }
            catch (InsufficientExecutionStackException)
            {
                clone = default;
                return false;
            }
        }
    }
}
