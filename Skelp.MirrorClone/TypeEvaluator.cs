﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone
{
    internal class TypeEvaluator
    {
        private static readonly Dictionary<Type, bool> cachedResults = new Dictionary<Type, bool>();

        public static bool IsReferenceOrContainsReferences(Type type)
        {
            if (cachedResults.TryGetValue(type, out bool value))
            {
                return value;
            }

            var method = typeof(RuntimeHelpers)
                .GetMethod(nameof(RuntimeHelpers.IsReferenceOrContainsReferences))
                .MakeGenericMethod(type);

            value = (bool)method.Invoke(null, null);

            cachedResults.Add(type, value);
            return value;
        }

        public static bool IsStructWithoutReferenceTypes(Type type)
        {
            return !IsReferenceOrContainsReferences(type);
        }
    }
}
