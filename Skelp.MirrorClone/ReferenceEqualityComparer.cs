﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone
{
    internal sealed class ReferenceEqualityComparer : IEqualityComparer<object>
    {
        private static readonly ReferenceEqualityComparer instance = new ReferenceEqualityComparer();

        public static ReferenceEqualityComparer Instance => instance;

        public bool Equals(object left, object right)
        {
            return ReferenceEquals(left, right);
        }

        public int GetHashCode(object value)
        {
            return RuntimeHelpers.GetHashCode(value);
        }
    }
}
