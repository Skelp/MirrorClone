﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using Serilog;
using Skelp.MirrorClone.Benchmark;
using System.IO;

public class Program
{
    public static void Main(string[] args)
    {
        var dir = @"F:\repos\MirrorClone\Skelp.MirrorClone.Benchmark\bin\log.txt";
        Logger.Path = dir;

        BenchmarkRunner.Run<Benchmark>(ManualConfig
                .Create(DefaultConfig.Instance)
                .WithOptions(ConfigOptions.DisableOptimizationsValidator));
    }
}

