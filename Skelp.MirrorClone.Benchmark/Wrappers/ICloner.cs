﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public interface ICloner
    {
        T Clone<T>(T obj, [CallerMemberName] string member = null);
    }
}
