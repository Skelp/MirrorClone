﻿using Skelp.MirrorClone.Extensions;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public class MirrorClone : ICloner
    {
        public static MirrorClone Instance = new MirrorClone();

        public T Clone<T>(T obj, [CallerMemberName] string member = null)
        {
            try
            {
                return obj.MirrorClone();
            }
            catch (System.Exception ex)
            {
                Logger.Write(ex.ToString());
                return default;
            }
        }

        public override string ToString() => nameof(MirrorClone);
    }
}
