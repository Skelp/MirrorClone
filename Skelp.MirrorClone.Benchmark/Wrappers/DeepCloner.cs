﻿using Force.DeepCloner;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public class DeepCloner : ICloner
    {
        public T Clone<T>(T obj, [CallerMemberName] string member = null)
        {
            return obj.DeepClone();
        }

        public override string ToString() => nameof(DeepCloner);

    }
}
