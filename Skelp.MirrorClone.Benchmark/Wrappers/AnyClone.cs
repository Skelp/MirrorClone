﻿using AnyClone;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public class AnyClone : ICloner
    {
        public T Clone<T>(T obj, [CallerMemberName] string member = null)
        {
            return obj.Clone();
        }

        public override string ToString() => nameof(AnyClone);
    }
}
