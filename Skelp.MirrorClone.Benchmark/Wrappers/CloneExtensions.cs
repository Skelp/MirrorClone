﻿using System.Runtime.CompilerServices;
using CloneExtensions;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public class CloneExtensions : ICloner
    {
        public T Clone<T>(T obj, [CallerMemberName] string member = null)
        {
            return obj.GetClone();
        }

        public override string ToString() => nameof(CloneExtensions);
    }
}
