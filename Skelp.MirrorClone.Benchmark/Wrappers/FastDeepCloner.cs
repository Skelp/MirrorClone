﻿using FastDeepCloner;
using System.Runtime.CompilerServices;

namespace Skelp.MirrorClone.Benchmark.Wrappers
{
    public class FastDeepCloner : ICloner
    {
        public T Clone<T>(T obj, [CallerMemberName] string member = null)
        {
            return (T)obj.Clone();
        }

        public override string ToString() => nameof(FastDeepCloner);

    }
}
