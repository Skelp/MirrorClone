﻿using NUnit.Framework;
using Serilog;
using Skelp.MirrorClone.Benchmark.Wrappers;
using Skelp.MirrorClone.Tests.TestTypes;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Skelp.MirrorClone.Benchmark
{
    internal class BaseMethods
    {
        public static int Integer(ICloner cloner, int i)
        {
            try
            {
                return cloner.Clone(i);
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(Integer)}, {i}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static string String(ICloner cloner, string str)
        {
            try
            {
                return cloner.Clone(str);
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(String)}, {str}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static object Null(ICloner cloner)
        {
            try
            {
                return cloner.Clone((object)null);
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(Null)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<SimpleClass> SimpleClasses(ICloner cloner)
        {
            try
            {
                var objects = new List<SimpleClass>();
                objects.Add(new SimpleClass() { Array = null });
                objects.Add(new SimpleClass() { Array = new string[0] });
                objects.Add(new SimpleClass() { Array = new string[] { "Hello", "World" } });

                var results = new List<SimpleClass>();

                foreach (var item in objects)
                {
                    var result = cloner.Clone(item);
                    results.Add(result);

                    if (item.Array != null)
                    {
                        Assert.That(ReferenceEquals(item.Array, result.Array) == false);
                        Assert.That(item.Array.Length, Is.EqualTo(result.Array.Length));
                    }
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(SimpleClasses)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<SimpleImmutableStruct> SimpleImmutableStructs(ICloner cloner)
        {
#if RELEASE
            if (cloner is Wrappers.FastDeepCloner)
            {
                return default;
            }
#endif
            try
            {
                var structs = new List<SimpleImmutableStruct>();
                structs.Add(new SimpleImmutableStruct());
                structs.Add(new SimpleImmutableStruct(DateTime.Now, 1));

                var results = new List<SimpleImmutableStruct>();

                foreach (var item in structs)
                {
                    results.Add(cloner.Clone(item));
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(SimpleImmutableStructs)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<NestedRefStruct> NestedRefStructs(ICloner cloner)
        {
#if RELEASE
            if (cloner is Wrappers.FastDeepCloner
                || cloner is Wrappers.AnyClone
                || cloner is Wrappers.DeepCloner
                || cloner is Wrappers.CloneExtensions)
            {
                return default;
            }
#endif
            try
            {
                var structs = new List<NestedRefStruct>();
                structs.Add(new NestedRefStruct());
                structs.Add(new NestedRefStruct(new RefMemberStruct(new SimpleClass())));
                structs.Add(new NestedRefStruct(new RefMemberStruct(new SimpleClass() { Array = new string[] { "Hello", "World" } })));

                var results = new List<NestedRefStruct>();

                foreach (var item in structs)
                {
                    var result = cloner.Clone(item);
                    results.Add(result);

                    if (item.refStruct.SimpleClassRef != null)
                    {
                        Assert.That(ReferenceEquals(result.refStruct.SimpleClassRef, item.refStruct.SimpleClassRef) == false);

                        if (item.refStruct.SimpleClassRef.Array != null)
                        {
                            Assert.That(ReferenceEquals(result.refStruct.SimpleClassRef.Array, item.refStruct.SimpleClassRef.Array) == false);
                        }
                    }
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(NestedRefStructs)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<object> Arrays(ICloner cloner)
        {
            try
            {
                var objects = new List<object>();
                objects.Add(new string[] { "look", "mum", "no", "errors" });
                objects.Add(new int[] { 1, 2, 3, 4, 5 });

                var results = new List<object>();

                foreach (var item in objects)
                {
                    results.Add(cloner.Clone(item));
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(Arrays)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<object> MultiDimArrays(ICloner cloner)
        {
            try
            {
                var objects = new List<object>();
                objects.Add(new int[3, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
                objects.Add(new int[3, 2, 1] { { { 1 }, { 2 } }, { { 3 }, { 4 } }, { { 5 }, { 6 } } });

                var results = new List<object>();

                foreach (var item in objects)
                {
                    var result = cloner.Clone(item);
                    results.Add(result);
                    Assert.That(ReferenceEquals(result, item) == false);
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(MultiDimArrays)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static IEnumerable<object> RaggedArrays(ICloner cloner)
        {
            try
            {
                var objects = new List<object>();
                objects.Add(new int[3, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 } });

                var results = new List<object>();

                foreach (var item in objects)
                {
                    var result = cloner.Clone(item);
                    results.Add(result);
                    Assert.That(ReferenceEquals(result, item) == false);
                }

                return results;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(RaggedArrays)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static ParentChildClass ParentChildClass(ICloner cloner)
        {
#if RELEASE
            if (cloner is Wrappers.FastDeepCloner
                || cloner is Wrappers.AnyClone)
            {
                return default;
            }
#endif
            try
            {
                var parentClass = new ParentChildClass();
                parentClass.Children = new List<ParentChildClass>();
                parentClass.Children.Add(new ParentChildClass { Parent = parentClass });
                parentClass.Children.Add(new ParentChildClass { Parent = parentClass });
                parentClass.Children[0].Children = new List<ParentChildClass>();
                parentClass.Children[0].Children.Add(new ParentChildClass { Parent = parentClass.Children[0] });

                var result = cloner.Clone(parentClass);

                Assert.That(ReferenceEquals(result, result.Children[0].Parent));
                Assert.That(ReferenceEquals(result.Children[0], result.Children[0].Children[0].Parent));

                return result;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(ParentChildClass)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }

        public static dynamic DynamicExpando(ICloner cloner)
        {
#if RELEASE
            if (cloner is Wrappers.FastDeepCloner
                || cloner is Wrappers.CloneExtensions)
            {
                return default;
            }
#endif
            try
            {
                dynamic dyn = new ExpandoObject();
                dyn.ClassRef = new SimpleClass() { Array = new string[] { "Hello", "World" } };
                dyn.MyInt = 3;

                var clone = cloner.Clone(dyn);

                Assert.That(dyn.ClassRef.Array.Length, Is.EqualTo(clone.ClassRef.Array.Length));
                Assert.That(dyn.MyInt, Is.EqualTo(clone.MyInt));

                return clone;
            }
            catch (Exception)
            {
                Logger.Write($"{cloner.ToString()}, {nameof(DynamicExpando)}");
#if DEBUG
                throw;
#else
                return default;
#endif
            }
        }
    }
}
