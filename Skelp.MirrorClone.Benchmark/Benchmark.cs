﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Order;
using Serilog;
using Skelp.MirrorClone.Benchmark.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Skelp.MirrorClone.Benchmark
{
#if RELEASE
    [SimpleJob(RuntimeMoniker.NetCoreApp31/*, invocationCount: 512*/)]
    [SimpleJob(RuntimeMoniker.Net50/*, invocationCount: 512*/)]
    [SimpleJob(RuntimeMoniker.Net60/*, invocationCount: 512*/)]
#else
    [DryJob(RuntimeMoniker.Net60)]
    [DryJob(RuntimeMoniker.Net50)]
    [DryJob(RuntimeMoniker.NetCoreApp31)]
#endif
    [MarkdownExporter]
    [MarkdownExporterAttribute.GitHub]
    //[DryJob]
    [Orderer(SummaryOrderPolicy.FastestToSlowest, MethodOrderPolicy.Declared)]
    [MemoryDiagnoser]
    [GroupBenchmarksBy(BenchmarkLogicalGroupRule.ByMethod)]
    public class Benchmark
    {
        private readonly Consumer consumer = new Consumer();

        [GlobalSetup]
        public void Setup()
        {
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public void SimpleClasses(ICloner cloner)
        {
            BaseMethods.SimpleClasses(cloner)?.Consume(consumer);
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public void SimpleImmutableStructs(ICloner cloner)
        {
            BaseMethods.SimpleImmutableStructs(cloner)?.Consume(consumer);
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public void NestedRefStructs(ICloner cloner)
        {
            BaseMethods.NestedRefStructs(cloner)?.Consume(consumer);
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public void Arrays(ICloner cloner)
        {
            BaseMethods.Arrays(cloner)?.Consume(consumer);
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public void MultiDimensionalArrays(ICloner cloner)
        {
            BaseMethods.MultiDimArrays(cloner)?.Consume(consumer);
        }

        [Benchmark]
        [ArgumentsSource(nameof(GetClonerTypes))]
        public object ParentChildClass(ICloner cloner)
        {
            return BaseMethods.ParentChildClass(cloner);
        }

        //[Benchmark]
        //[ArgumentsSource(nameof(GetClonerTypes))]
        //public object DynamicExpando(ICloner cloner)
        //{
        //    try
        //    {
        //        return BaseMethods.DynamicExpando(cloner);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Write(ex.ToString());
        //        return null;
        //    }
        //}

        public IEnumerable<ICloner> GetClonerTypes()
        {
            var type = typeof(ICloner);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface)
                .Select(x => (ICloner)Activator.CreateInstance(x));
        }
    }
}