﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Skelp.MirrorClone.Benchmark
{
    internal static class Logger
    {
        public static string Path { get; set; }

        public static void Write(string msg)
        {
            File.AppendAllText(Path, msg);
        }
    }
}
