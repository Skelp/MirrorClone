﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public class SimpleClass : IEquatable<SimpleClass>
    {
        public string[] Array { get; set; } = new string[] { "test", "class" };

        public override bool Equals(object? obj)
        {
            return base.Equals(obj);
        }

        public bool Equals(SimpleClass? other)
        {
            if (this is null && other is null)
            {
                return true;
            }
            else if ((this is null) != (other is null))
            {
                return false;
            }

            return Array.SequenceEqual(other.Array);
        }

        public override string ToString()
        {
            return $"Array={(Array == null ? "NULL" : Array.Length)}";
        }
    }
}
