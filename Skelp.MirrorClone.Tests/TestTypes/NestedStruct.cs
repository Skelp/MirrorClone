﻿using System.Diagnostics.CodeAnalysis;

namespace Skelp.MirrorClone.Tests.TestTypes
{
    public readonly struct NestedStruct
    {
        public int MyInt { get; }
        public NestedStruct[] Children { get; }

        public NestedStruct(NestedStruct[] children, int myInt)
        {
            Children = children;
            MyInt = myInt;
        }

        public NestedStruct()
        {
            MyInt = 1;

            Children = new[]
            {
                new NestedStruct(new NestedStruct[0], 2)
            };
        }

        public override bool Equals([NotNullWhen(true)] object? obj)
        {
            var casted = (NestedStruct)obj;
            return casted.MyInt.Equals(MyInt) && casted.Children.SequenceEqual(Children);
        }
    }
}
