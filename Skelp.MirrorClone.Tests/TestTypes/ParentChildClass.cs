﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public class ParentChildClass
    {
        public ParentChildClass Parent { get; set; }

        public List<ParentChildClass> Children { get; set; }
    }
}
