﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public readonly struct RefMemberStruct
    {
        public readonly SimpleClass SimpleClassRef;

        public RefMemberStruct(SimpleClass simpleClassRef)
        {
            SimpleClassRef = simpleClassRef;
        }

        public RefMemberStruct()
        {
            SimpleClassRef = null;
        }
    }
}
