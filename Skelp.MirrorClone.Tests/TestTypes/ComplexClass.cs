﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public class ComplexClass : IEquatable<ComplexClass>
    {
        public Func<int> Operation { get; set; }

        public NestedClass NestedClass { get; }

        public List<SimpleClass> TestClasses { get; set; }

        public int FuncReturnInt { get; set; }

        public ComplexClass(bool initialize)
        {
            if (initialize)
            {
                TestClasses = new List<SimpleClass>();
                TestClasses.Add(new SimpleClass { Array = new[] { "henlo", "tests" } });
                TestClasses.Add(new SimpleClass { Array = new[] { "Tests", "are", "important" } });

                NestedClass = new NestedClass() { Name = "Parent" };
                NestedClass.NestedTestClass = new NestedClass() { Name = "Child" };
                Operation = () => FuncReturnInt;
                FuncReturnInt = 1;
            }
        }

        public override bool Equals(object? obj)
        {
            return base.Equals(obj);
        }

        public bool Equals(ComplexClass? other)
        {
            if (this is null && other is null)
            {
                return true;
            }

            else if ((this is null) != (other is null))
            {
                return false;
            }

            if (TestClasses.SequenceEqual(other.TestClasses) == false)
            {
                return false;
            }

            if (this.NestedClass is null && other.NestedClass is null)
            {
                return true;
            }

            else if ((this.NestedClass is null) != (other.NestedClass is null))
            {
                return false;
            }

            return other.NestedClass.Equals(NestedClass);
        }
    }
}
