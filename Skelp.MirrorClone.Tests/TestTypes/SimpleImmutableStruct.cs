﻿using System.Diagnostics.CodeAnalysis;

namespace Skelp.MirrorClone.Tests.TestTypes
{
    public readonly struct SimpleImmutableStruct
    {
        public DateTime DateTime { get; }

        public double Double { get; }

        public SimpleImmutableStruct(DateTime dateTime, double @double)
        {
            DateTime = dateTime;
            Double = @double;
        }

        public SimpleImmutableStruct()
        {
            DateTime = default;
            Double = default;
        }

        public override bool Equals([NotNullWhen(true)] object? obj)
        {
            var casted = (SimpleImmutableStruct)obj;
            return DateTime.Equals(casted.DateTime) && Double.Equals(casted.Double);
        }
    }
}
