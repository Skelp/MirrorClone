﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public readonly struct NestedRefStruct
    {
        public readonly RefMemberStruct refStruct;

        public NestedRefStruct()
        {
            refStruct = new RefMemberStruct();
        }

        public NestedRefStruct(RefMemberStruct refStruct)
        {
            this.refStruct = refStruct;
        }
    }
}
