﻿namespace Skelp.MirrorClone.Tests.TestTypes
{
    public class NestedClass : IEquatable<NestedClass>
    {
        public string Name { get; set; }

        public NestedClass NestedTestClass { get; set; }

        public bool Equals(NestedClass? other)
        {
            if (this is null && other is null)
            {
                return true;
            }

            else if ((this is null) != (other is null))
            {
                return false;
            }

            if (other?.Name.Equals(Name) == false)
            {
                return false;
            }

            if (this.NestedTestClass is null && other.NestedTestClass is null)
            {
                return true;
            }

            else if ((this.NestedTestClass is null) != (other.NestedTestClass is null))
            {
                return false;
            }

            return Name == other?.Name && NestedTestClass.Equals(other.NestedTestClass);
        }
    }
}
