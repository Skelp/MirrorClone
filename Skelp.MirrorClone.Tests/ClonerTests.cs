using NUnit.Framework;
using Skelp.MirrorClone.Extensions;
using Skelp.MirrorClone.Tests.TestTypes;
using System.Dynamic;

namespace Skelp.MirrorClone.Tests
{
    [TestFixture]
    public class ClonerTests
    {
        [TestCase(1)]
        [TestCase((long)1)]
        [TestCase('T')]
        public void Clone_Primitive<T>(T primitive)
        {
            var clone = primitive.MirrorClone();

            Assert.That(clone, Is.EqualTo(primitive));
        }

        public void Clone_String()
        {
            var testString = "This is a test string";

            var clone = testString.MirrorClone();

            Assert.That(clone, Is.EqualTo(testString));
        }

        [TestCase("look", "mum", "no", "errors")]
        public void Clone_Array(params string[] obj)
        {
            var clone = obj.MirrorClone();

            Assert.That(clone, Is.EqualTo(obj));
        }

        [TestCase("look", "mum", "no", "errors")]
        public void Clone_List(params string[] obj)
        {
            var list = obj.ToList();

            var clone = list.MirrorClone();

            Assert.That(clone, Is.EqualTo(list));
        }

        [TestCase("look", "mum", "no", "errors")]
        public void Clone_Dictionary(params string[] obj)
        {
            var dict = obj.ToDictionary(x => x, y => y);

            var clone = dict.MirrorClone();

            Assert.That(clone, Is.EqualTo(dict));
        }

        [Test]
        public void Clone_ComplexObjectWithFunc()
        {
            var complexClass = new ComplexClass(true);

            var clone = complexClass.MirrorClone();
            Assert.That(clone, Is.EqualTo(complexClass));

            // Change the clone to prove that the delegate in clone
            // is referencing a new object, and not the old complexClass instance
            // as that would imply a cheap shallow clone, instead of a true clone
            clone.FuncReturnInt = 2;

            Assert.That(clone.Operation.Invoke(), Is.Not.EqualTo(complexClass.Operation.Invoke()));
        }

        [Test]
        public void Clone_ParentChild()
        {
            var parentClass = new ParentChildClass();
            parentClass.Children = new List<ParentChildClass>();
            parentClass.Children.Add(new ParentChildClass { Parent = parentClass });
            parentClass.Children.Add(new ParentChildClass { Parent = parentClass });
            parentClass.Children[0].Children = new List<ParentChildClass>();
            parentClass.Children[0].Children.Add(new ParentChildClass { Parent = parentClass.Children[0] });

            var clone = parentClass.MirrorClone();

            Assert.That(ReferenceEquals(clone, clone.Children[0].Parent));
            Assert.That(ReferenceEquals(clone.Children[0], clone.Children[0].Children[0].Parent));
        }

        [Test]
        public void Clone_Dynamic()
        {
            dynamic expObj = new ExpandoObject();
            expObj.TestProp = "This do be a string tho";

            dynamic childExpObj = new ExpandoObject();
            childExpObj.MyInt = 1;

            expObj.Child = childExpObj;

            var clone = (dynamic)((object)expObj).MirrorClone();

            Assert.That(clone.TestProp, Is.EqualTo(expObj.TestProp));
            Assert.That(clone.Child.MyInt, Is.EqualTo(childExpObj.MyInt));
        }

        [Test]
        public void Clone_OtherDynamic()
        {
            dynamic dyn = new ExpandoObject();
            dyn.ClassRef = new SimpleClass() { Array = new string[] { "Hello", "World" } };
            dyn.MyInt = 3;

            var clone = (dynamic)((object)dyn).MirrorClone();
            Assert.That(dyn.ClassRef.Array.Length, Is.EqualTo(clone.ClassRef.Array.Length));
            Assert.That(dyn.MyInt, Is.EqualTo(clone.MyInt));
        }

        [Test]
        public void Clone_Null()
        {
            var clone = ((object)null).MirrorClone();

            Assert.That(clone, Is.Null);
        }

        [Test]
        public unsafe void Clone_SimpleImmutableStruct()
        {
            var testStruct = new SimpleImmutableStruct();

            var clone = testStruct.MirrorClone();

            var testStructPtr = &testStruct;
            var clonePtr = &clone;
            Assert.That(clone, Is.EqualTo(testStruct));
            Assert.That(testStructPtr != clonePtr);
        }

        [Test]
        public void Clone_NestedRefStruct()
        {
            var testStruct = new NestedRefStruct();

            var clone = testStruct.MirrorClone();

            Assert.That(clone, Is.EqualTo(testStruct));
        }

        [Test]
        public void Clone_NestedStruct()
        {
            var testStruct = new NestedStruct();

            var clone = testStruct.MirrorClone();

            Assert.That(clone, Is.EqualTo(testStruct));
        }

        [Test]
        public void Clone_MultiDimArray2D()
        {
            var arr = new int[3, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 } };

            var clone = arr.MirrorClone();

            CollectionAssert.AreEqual(arr, clone);
        }

        [Test]
        public void Clone_MultiDimArray3D()
        {
            var arr = new int[3, 2, 1] { { { 1 }, { 2 } }, { { 3 }, { 4 } }, { { 5 }, { 6 } } };

            var clone = arr.MirrorClone();

            CollectionAssert.AreEqual(arr, clone);
        }

        [Test]
        public void Clone_JaggedArray()
        {
            var arr = new int[3][] { new[] { 1, 2 }, new[] { 3, 4 }, new[] { 5, 6 } };

            var clone = arr.MirrorClone();

            CollectionAssert.AreEqual(arr, clone);
        }
    }
}